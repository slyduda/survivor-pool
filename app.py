from quart import Quart, make_response, render_template, request, jsonify
from custom_quart import CustomQuart
import sqlite3

app = CustomQuart(__name__)
conn = sqlite3.connect('survivor.db')
c = conn.cursor()

@app.route('/', methods=['GET'])
async def get_home():
    return await render_template('index.html')

@app.route('/room', methods=['GET'])
async def type_room():
    room = request.cookies.get('room')
    key = request.cookies.get('key')
    return await render_template('play.html')

@app.route('/room', methods=['POST'])
async def submit_room():
    values = request.get_json()
    print(values)
    room = values['room']
    password = values['password']
    query = 'SELECT id, pp_amount FROM rooms WHERE pass={},room={}'.format(room, password)
    c.execute(query)
    conn.commit()
    conn.close()
    response = {}
    if not query:
        response = {
            'message': "Incorrect room and key combo.",
            'room_id': query[0][0],
            'pp_amount': query[0][1]
        }
    return jsonify(response), 200

@app.route('/player', methods=['GET'])
async def get_player():
    values = request.get_json()
    print(values)
    room_id = values['room_id']
    player = None
    #player = request.cookies.get('player')
    query = 'SELECT nickname FROM players WHERE room_id={}'.format(room_id)
    c.execute(query)
    conn.commit()
    conn.close()
    response = {}
    if not player:
        response = {
            'message': "Select your nickname or create a new one.",
            'players': query[0]
        }   
    else:
        response = {
            'message': "Select your nickname or create a new one.",
            'players': query[0]
        }
    return jsonify(response), 200

@app.route('/player', methods=['POST'])
async def add_player():
    values = request.get_json()
    print(values)
    nickname = values['nickname']
    room_id = values['room_id']
    query = 'INSERT INTO players(nickname, room_id) VALUES({}, {})'.format(nickname, room_id)
    c.execute(query)
    conn.commit()
    conn.close()
    response = {
        'message': "{}, make your selections.".format(nickname),
        'players': query[0]
    }
    return jsonify(response), 200

@app.route('/selection', methods=['GET'])
async def get_selections():
    values = request.get_json()
    player_id = values['player_id']
    room_id = values['room_id']
    pp_amount = values['pp_amount']
    query = 'SELECT contestant_id, player_id FROM selections WHERE type=1, room_id={}'.format( room_id)
    c.execute(query)
    conn.commit()
    conn.close()
    response = {
        'choices': query
    }
    return jsonify(response), 200

@app.route('/selection', methods=['POST'])
async def make_selections():
    values = request.get_json()
    contestants = values['choices']
    player_id = values['player_id']
    room_id = values['room_id']
    for index in contestants:
        query = 'INSERT INTO selections(type, contestant_id, player_id, room_id) VALUES(1, {}, {}, {})'.format(index, player_id, room_id)
        c.execute(query)
        conn.commit()
        conn.close()
    response = {
        'message': "Responses entered!"
    }
    return jsonify(response), 200
    
if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('-p', '--port', type=int, default=5000)
    args = parser.parse_args()
    port = args.port
    app.run(host='0.0.0.0', port=port, debug=True)